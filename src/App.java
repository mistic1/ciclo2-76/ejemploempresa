public class App {
    public static void main(String[] args) throws Exception {
        
        //Persona Objpersona = new Persona("Carlos", 35);
        Empleado objEmpleado = new Empleado("Juan", 45, 1600000);    
        System.out.println(objEmpleado.mostrar());
        System.out.println("Su Salario neto es: $"+objEmpleado.calcularSalarioNeto());
        System.out.println("Su descuento en Salud es de: $"+objEmpleado.getSalud());
        System.out.println("Su descuento en Pension es de: $"+objEmpleado.getPension());
        System.out.println("Su subsidio en transporte es de: $"+objEmpleado.getSubTranporte());
    }
}
