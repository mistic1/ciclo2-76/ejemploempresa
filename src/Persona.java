import java.util.stream.Stream;

public class Persona {
    /****
     * Atributos
     ****/
    private String nombre;
    private int edad;

    /***
     * Constructor
     ***/
    public Persona(String nombre, int edad){
        this.nombre = nombre;
        this.edad = edad;
    }
    /***
     * Metodos
     ***/
    public String mostrar(){
        return ("Nombre: "+nombre+" - Edad: "+edad);
    }
    // public int getEdad(){
    //     return this.edad;
    // }
    // public String getNombre(){
    //     return nombre;
    // }
    // public void setNombre(String newNombre){
    //     this.nombre = newNombre;
    // }
}

