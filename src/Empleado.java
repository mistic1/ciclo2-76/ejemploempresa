public class Empleado extends Persona{
    /****
     * Atributos
     ****/
    private double sueldoBruto;
    private double salud;
    private double pension;
    private double subTransporte;

 /***
     * Constructor
     ***/
    public Empleado(String nombre, int edad, double sueldoBruto){
        super(nombre, edad);
        this.sueldoBruto = sueldoBruto;
        this.salud = salud;
        this.pension = pension;
        this.subTransporte = subTransporte;
    }
    /***
     * Metodos
     ***/
    public double calcularSalarioNeto(){
        //sueldoBruto = 1000000;
        pension = sueldoBruto*0.04;
        salud = sueldoBruto*0.04;
        subTransporte = sueldoBruto*0.061;
        double salarioNeto = sueldoBruto+subTransporte-pension-salud;
        return salarioNeto;
    }
    public double getSalud(){
        salud = sueldoBruto*0.04;
        return this.salud;
    } 
    public double getPension(){
        pension = sueldoBruto*0.04;
        return this.pension;
    }
    public double getSubTranporte(){
        subTransporte = sueldoBruto*0.061;
        return this.subTransporte;
    }
}
